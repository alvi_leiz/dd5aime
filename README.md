# Foundry VTT - AIME

An implementation of the Adventures in Middle Earth 5th Edition game system for [Foundry Virtual Tabletop](http://foundryvtt.com).

The software component of this system is distributed under the GNUv3 license while the game content is distributed
under the Open Gaming License v1.0a.

The [Systems Reference Document (SRD)](http://media.wizards.com/2016/downloads/DND/SRD-OGL_V5.1.pdf) for included
content is available in full from Wizards of the Coast.

## Installation Instructions

To install the AIME system for Foundry Virtual Tabletop, simply paste the following URL into the **Install System**
dialog on the Setup menu of the application.

https://gitlab.com/alvi_leiz/dd5aime/raw/master/system.json

If you wish to manually install the system, you must clone or extract it into the ``Data/systems/aime`` folder. You
may do this by cloning the repository or downloading a zip archive from the
[Releases Page](https://gitlab.com/foundrynet/dnd5e/-/releases).
